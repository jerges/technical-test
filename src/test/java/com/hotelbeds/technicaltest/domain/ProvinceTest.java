package com.hotelbeds.technicaltest.domain;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
class ProvinceTest {

    @Test
    public void getName() {
        assertEquals("(Baleares)", Province.BAL.getDescription());
        assertEquals("(Canarias)", Province.CAN.getDescription());
        assertEquals("(Ceuta y Melilla)", Province.CYM.getDescription());
        assertEquals("(Resto)", Province.RES.getDescription());
        assertEquals("(Teruel)", Province.TER.getDescription());

    }

    @Test
    public void getTax() {
        assertEquals(BigDecimal.valueOf(18.3), Province.BAL.getTax());
        assertEquals(BigDecimal.valueOf(4.7), Province.CAN.getTax());
        assertEquals(BigDecimal.valueOf(8.1), Province.CYM.getTax());
        assertEquals(BigDecimal.valueOf(0.5), Province.TER.getTax());
        assertEquals(BigDecimal.valueOf(21.5), Province.RES.getTax());
    }


    @Test
    public void getTotalAmount() {
        assertEquals(BigDecimal.valueOf(2721, 2), Province.BAL.getTotalAmount(BigDecimal.valueOf(23)));
        assertEquals(BigDecimal.valueOf(2408, 2), Province.CAN.getTotalAmount(BigDecimal.valueOf(23)));
        assertEquals(BigDecimal.valueOf(2486, 2), Province.CYM.getTotalAmount(BigDecimal.valueOf(23)));
        assertEquals(BigDecimal.valueOf(2312, 2), Province.TER.getTotalAmount(BigDecimal.valueOf(23)));
        assertEquals(BigDecimal.valueOf(2795, 2), Province.RES.getTotalAmount(BigDecimal.valueOf(23)));
    }


}
