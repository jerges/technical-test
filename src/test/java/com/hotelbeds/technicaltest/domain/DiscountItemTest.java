package com.hotelbeds.technicaltest.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;


@RunWith(MockitoJUnitRunner.class)
public class DiscountItemTest {

    private static final DiscountItem DISCOUNT_ITEM = new DiscountItem(new DiscountRange(BigDecimal.ZERO, BigDecimal.TEN), 3);

    @Test()
    public void equalsVerifier() {
        EqualsVerifier.forClass(DiscountItem.class).suppress(Warning.STRICT_INHERITANCE).verify();
    }

    @Test
    public void shouldContainsValuesInToString() {
        assertThat(DISCOUNT_ITEM.toString()).containsIgnoringCase(String.valueOf(DISCOUNT_ITEM.getDiscountRange()));
        assertThat(DISCOUNT_ITEM.toString()).containsIgnoringCase(String.valueOf(DISCOUNT_ITEM.getDiscount()));
    }

    @Test
    public void compareTo() {
        final DiscountItem discountItem = new DiscountItem(new DiscountRange(BigDecimal.ONE, BigDecimal.TEN), 2);
        assertEquals(1, DISCOUNT_ITEM.compareTo(discountItem));
    }


}
