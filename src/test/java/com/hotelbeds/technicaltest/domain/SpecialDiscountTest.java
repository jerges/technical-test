package com.hotelbeds.technicaltest.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class SpecialDiscountTest {

    @Test
    public void getProductName() {
        assertEquals("tobacco", SpecialDiscount.TOBACCO.getProductName());
    }

    @Test
    public void getSpecialDiscount() {
        assertEquals(Integer.valueOf(50), SpecialDiscount.TOBACCO.getSpecialDiscount());
    }

}
