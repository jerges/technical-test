package com.hotelbeds.technicaltest.service.resolver.impl;


import com.hotelbeds.technicaltest.config.DiscountConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class DiscountResolverImplTest {

    private DiscountResolverImpl discountResolver;

    private static final DiscountConfiguration DISCOUNT_CONFIGURATION = new DiscountConfiguration();

    @Before
    public void setUp() {
        discountResolver = new DiscountResolverImpl(DISCOUNT_CONFIGURATION.discountMap());
    }

    @Test
    public void getDiscount3Percentage() {
        final String itemDescription = "jhon wick dog";
        final BigDecimal amount = BigDecimal.valueOf(12);
        assertEquals(3, discountResolver.getDiscount(amount, itemDescription));
    }

    @Test
    public void getDiscount7Percentage() {
        final String itemDescription = "jhon wick dog";
        final BigDecimal amount = BigDecimal.valueOf(1001);
        assertEquals(7, discountResolver.getDiscount(amount, itemDescription));
    }

    @Test
    public void getDiscount15Percentage() {
        final String itemDescription = "jhon wick dog";
        final BigDecimal amount = BigDecimal.valueOf(2001);
        assertEquals(15, discountResolver.getDiscount(amount, itemDescription));
    }

    @Test
    public void getDiscount19Percentage() {
        final String itemDescription = "jhon wick dog";
        final BigDecimal amount = BigDecimal.valueOf(6000);
        assertEquals(19, discountResolver.getDiscount(amount, itemDescription));
    }

    @Test
    public void getDiscount25Percentage() {
        final String itemDescription = "jhon wick dog";
        final BigDecimal amount = BigDecimal.valueOf(8000);
        assertEquals(25, discountResolver.getDiscount(amount, itemDescription));
    }


    @Test
    public void getDiscountLimitRange() {
        final String itemDescription = "jhon wick dog";
        final BigDecimal amount = BigDecimal.valueOf(2000);
        assertEquals(7, discountResolver.getDiscount(amount, itemDescription));
    }

    @Test
    public void getDiscountLimitRangeForSorted() {
        final String itemDescription = "jhon wick dog";
        final BigDecimal amount = BigDecimal.valueOf(5000);
        assertEquals(15, discountResolver.getDiscount(amount, itemDescription));
    }


    @Test
    public void specialDiscountByProduct() {
        final String itemDescription = "tobacco";
        final BigDecimal amount = BigDecimal.valueOf(12);
        assertEquals(50, discountResolver.getDiscount(amount, itemDescription));
    }
}
