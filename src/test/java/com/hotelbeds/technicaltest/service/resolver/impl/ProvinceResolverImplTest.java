package com.hotelbeds.technicaltest.service.resolver.impl;


import com.hotelbeds.technicaltest.domain.Province;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ProvinceResolverImplTest {

    @InjectMocks
    private ProvinceResolverImpl provinceResolver;

    @Test
    public void provinceByCode() {
        assertEquals(Province.BAL, provinceResolver.provinceByCode("BAL"));
    }

    @Test
    public void provinceByUndefinedCode() {
        assertEquals(Province.UNDEFINED, provinceResolver.provinceByCode("VZL"));
    }
}
