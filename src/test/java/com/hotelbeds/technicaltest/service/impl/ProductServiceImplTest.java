package com.hotelbeds.technicaltest.service.impl;

import com.hotelbeds.technicaltest.domain.Province;
import com.hotelbeds.technicaltest.exception.ProvinceException;
import com.hotelbeds.technicaltest.service.resolver.DiscountResolver;
import com.hotelbeds.technicaltest.service.resolver.impl.ProvinceResolverImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private ProvinceResolverImpl provinceResolve;

    @Mock
    private DiscountResolver discountResolver;

    @Test
    public void calculateTotalAmountWithOutNumberItems() {
        final String itemDescription = "jhon wick dog";
        assertEquals(BigDecimal.valueOf(0), productService.calculateTotalAmount(itemDescription, BigDecimal.TEN, 0, Province.BAL.name()));
        verify(discountResolver, times(0)).getDiscount(any(), any());

    }

    @Test
    public void calculateTotalAmountWithTaxOnly() {
        final String itemDescription = "jhon wick dog";
        when(provinceResolve.provinceByCode(Province.BAL.name())).thenReturn(Province.BAL);
        assertEquals(BigDecimal.valueOf(46.85), productService.calculateTotalAmount(itemDescription, BigDecimal.TEN, 4, Province.BAL.name()));
        verify(discountResolver, times(1)).getDiscount(any(), any());

    }

    @Test
    public void calculateTotalAmountWithDiscount() {
        final String itemDescription = "jhon wick dog";
        when(provinceResolve.provinceByCode(Province.BAL.name())).thenReturn(Province.BAL);
        assertEquals(BigDecimal.valueOf(4684.68), productService.calculateTotalAmount(itemDescription, BigDecimal.valueOf(1000), 4, Province.BAL.name()));
        verify(discountResolver, times(1)).getDiscount(any(), any());
    }

    @Test
    public void calculateTotalAmountWithTaxByState() {
        final String state = Province.BAL.getDescription();
        when(provinceResolve.provinceByCode(state)).thenReturn(Province.BAL);
        assertEquals(BigDecimal.valueOf(94.64), productService.calculateTotalAmountWithTaxByState(state, BigDecimal.valueOf(80)));
        verify(provinceResolve, times(1)).provinceByCode(state);
    }

    @Test(expected = ProvinceException.class)
    public void calculateTotalAmountWithTaxByUndefinedState() {
        final String state = "ESP";
        when(provinceResolve.provinceByCode(state)).thenReturn(Province.UNDEFINED);
        productService.calculateTotalAmountWithTaxByState(state, BigDecimal.valueOf(80));
        verify(provinceResolve, times(1)).provinceByCode(state);

    }

    @Test
    public void calculateAmountWith50PercentageDiscount() {
        final BigDecimal amount = BigDecimal.valueOf(50);
        assertEquals(BigDecimal.valueOf(2500, 2), productService.calculateAmountWithDiscount(amount, 50));
    }
}
