package com.hotelbeds.technicaltest.controller;

import com.hotelbeds.technicaltest.exception.ProvinceException;
import com.hotelbeds.technicaltest.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc productControllerMock;

    @MockBean
    private ProductService productService;


    @Test
    public void itemPriceByState() throws Exception {
        when(productService.calculateTotalAmount("Jhon Wick", BigDecimal.valueOf(10), 1, "BAL")).thenReturn(BigDecimal.valueOf(11));
        final ResultActions result = productControllerMock.perform(get("/api/item-price-by-state").contentType(MediaType.APPLICATION_JSON)
                .param("itemDescription", "Jhon Wick")
                .param("itemPrice", "10")
                .param("numberOfItems", "1")
                .param("state", "BAL")).andExpect(status().isOk());
        assertEquals("11", result.andReturn().getResponse().getContentAsString());
    }


    @Test
    public void itemPriceByStateNotMapper() throws Exception {
        when(productService.calculateTotalAmount("Jhon Wick", BigDecimal.valueOf(10), 1, "VZL")).thenThrow(ProvinceException.class);
        final ResultActions result = productControllerMock.perform(get("/api/item-price-by-state").contentType(MediaType.APPLICATION_JSON)
                .param("itemDescription", "Jhon Wick")
                .param("itemPrice", "10")
                .param("numberOfItems", "1")
                .param("state", "VZL")).andExpect(status().isNotAcceptable());
    }


}
