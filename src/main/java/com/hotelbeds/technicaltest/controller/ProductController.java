package com.hotelbeds.technicaltest.controller;

import com.hotelbeds.technicaltest.exception.ProvinceException;
import com.hotelbeds.technicaltest.service.ProductService;
import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@Slf4j
@RestController
@RequestMapping("/api")
public class ProductController {

    private final ProductService productService;


    public ProductController(final ProductService productService) {
        this.productService = productService;
    }

    @Timed
    @GetMapping("/item-price-by-state")
    public ResponseEntity<BigDecimal> itemPriceByState(@RequestParam final String itemDescription, @RequestParam final BigDecimal itemPrice, @RequestParam final Integer numberOfItems, @RequestParam final String state) {
        log.debug("Calculate price by state");
        BigDecimal priceCalculated;
        try {
            priceCalculated = productService.calculateTotalAmount(itemDescription, itemPrice, numberOfItems, state);
        } catch (ProvinceException e) {
            MultiValueMap<String, String> headers = new HttpHeaders();
            return new ResponseEntity<>(BigDecimal.ZERO, headers, HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity<>(priceCalculated, HttpStatus.OK);
    }

}
