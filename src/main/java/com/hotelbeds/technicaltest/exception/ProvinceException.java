package com.hotelbeds.technicaltest.exception;

public class ProvinceException extends RuntimeException {

    public ProvinceException(final String message) {
        super(message);
    }
}
