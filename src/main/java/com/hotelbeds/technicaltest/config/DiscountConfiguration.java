package com.hotelbeds.technicaltest.config;

import com.hotelbeds.technicaltest.domain.DiscountItem;
import com.hotelbeds.technicaltest.domain.DiscountRange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Configuration
public class DiscountConfiguration {


    @Bean
    public Set<DiscountItem> discountMap() {
        final Set<DiscountItem> discountItems = new HashSet<>();
        discountItems.add(new DiscountItem(new DiscountRange(BigDecimal.valueOf(0), BigDecimal.valueOf(1000)), 3));
        discountItems.add(new DiscountItem(new DiscountRange(BigDecimal.valueOf(1000), BigDecimal.valueOf(2000)), 7));
        discountItems.add(new DiscountItem(new DiscountRange(BigDecimal.valueOf(5000), BigDecimal.valueOf(7500)), 19));
        discountItems.add(new DiscountItem(new DiscountRange(BigDecimal.valueOf(2000), BigDecimal.valueOf(5000)), 15));
        discountItems.add(new DiscountItem(new DiscountRange(BigDecimal.valueOf(7500), BigDecimal.valueOf(Double.MAX_VALUE)), 25));
        return discountItems;
    }


}
