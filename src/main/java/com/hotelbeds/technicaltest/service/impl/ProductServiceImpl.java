package com.hotelbeds.technicaltest.service.impl;

import com.hotelbeds.technicaltest.domain.Province;
import com.hotelbeds.technicaltest.exception.ProvinceException;
import com.hotelbeds.technicaltest.service.ProductService;
import com.hotelbeds.technicaltest.service.resolver.DiscountResolver;
import com.hotelbeds.technicaltest.service.resolver.ProvinceResolver;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProvinceResolver provinceResolver;

    private final DiscountResolver discountResolver;


    @Autowired
    public ProductServiceImpl(final ProvinceResolver provinceResolver, DiscountResolver discountResolver) {
        this.provinceResolver = provinceResolver;
        this.discountResolver = discountResolver;
    }


    @Override
    public BigDecimal calculateTotalAmount(final String itemDescription, @NonNull final BigDecimal itemPrice, @NonNull final Integer numberOfItems, @NonNull final String state) {
        if (numberOfItems > 0) {
            final BigDecimal numberOfItemsValid = BigDecimal.valueOf(numberOfItems);
            final BigDecimal totalPrice = itemPrice.multiply(numberOfItemsValid);
            final Integer discount = discountResolver.getDiscount(totalPrice, itemDescription);
            final BigDecimal totalAmountWIthDiscount = calculateAmountWithDiscount(totalPrice, discount > 0 ? discount : 1);
            return calculateTotalAmountWithTaxByState(state, totalAmountWIthDiscount);
        } else {
            return BigDecimal.ZERO;
        }
    }

    protected BigDecimal calculateTotalAmountWithTaxByState(final String state, final BigDecimal amount) {
        final Province province = provinceResolver.provinceByCode(state);
        if (Province.UNDEFINED.equals(province)) {
            throw new ProvinceException("Unmapped province...");
        }
        return province.getTotalAmount(amount);
    }

    protected BigDecimal calculateAmountWithDiscount(final BigDecimal amount, final Integer discount) {
        final BigDecimal discountAmount = amount.multiply(BigDecimal.valueOf(discount)).divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_UP);
        return amount.add(discountAmount.negate());
    }


}
