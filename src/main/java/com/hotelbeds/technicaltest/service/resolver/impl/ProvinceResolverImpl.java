package com.hotelbeds.technicaltest.service.resolver.impl;

import com.hotelbeds.technicaltest.domain.Province;
import com.hotelbeds.technicaltest.service.resolver.ProvinceResolver;
import org.springframework.stereotype.Component;

@Component
public class ProvinceResolverImpl implements ProvinceResolver {
    @Override
    public Province provinceByCode(final String state) {
        try {
            return Province.valueOf(state);
        } catch (IllegalArgumentException e) {
            return Province.UNDEFINED;
        }
    }
}
