package com.hotelbeds.technicaltest.service.resolver;

import com.hotelbeds.technicaltest.domain.Province;

public interface ProvinceResolver {

    Province provinceByCode(String state);

}
