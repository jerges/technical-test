package com.hotelbeds.technicaltest.service.resolver.impl;

import com.hotelbeds.technicaltest.domain.DiscountItem;
import com.hotelbeds.technicaltest.domain.SpecialDiscount;
import com.hotelbeds.technicaltest.service.resolver.DiscountResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Set;

@Component
public class DiscountResolverImpl implements DiscountResolver {
    private final Set<DiscountItem> discountItems;

    @Autowired
    public DiscountResolverImpl(final Set<DiscountItem> discountItems) {
        this.discountItems = discountItems;
    }

    @Override
    public Integer getDiscount(final BigDecimal amount, final String itemDescription) {
        final Integer discount = specialDiscountByProduct(itemDescription);
        if (discount == 0) {
            return discountItems.stream()
                    .sorted(Comparator.comparingInt(value -> discount))
                    .filter(discountItem -> rangeBetween(amount, discountItem))
                    .map(DiscountItem::getDiscount)
                    .findFirst().orElse(0);
        } else {
            return discount;
        }
    }

    private boolean rangeBetween(final BigDecimal amount, final DiscountItem discountItem) {
        return (amount.compareTo(discountItem.getDiscountRange().getFrom()) == 0 || amount.compareTo(discountItem.getDiscountRange().getFrom()) > 0) &&
                (amount.compareTo(discountItem.getDiscountRange().getTo()) == 0 || amount.compareTo(discountItem.getDiscountRange().getTo()) < 0);
    }


    private Integer specialDiscountByProduct(final String itemDescription) {
        if (SpecialDiscount.TOBACCO.getProductName().equals(itemDescription)) {
            return SpecialDiscount.TOBACCO.getSpecialDiscount();
        }
        return 0;
    }

}
