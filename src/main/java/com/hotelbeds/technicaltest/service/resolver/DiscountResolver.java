package com.hotelbeds.technicaltest.service.resolver;

import java.math.BigDecimal;

public interface DiscountResolver {

    Integer getDiscount(final BigDecimal amount, final String description);

}
