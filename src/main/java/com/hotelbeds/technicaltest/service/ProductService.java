package com.hotelbeds.technicaltest.service;


import java.math.BigDecimal;

public interface ProductService {

    BigDecimal calculateTotalAmount(String itemDescription, BigDecimal itemPrice, Integer numberOfItems, String state);

}
