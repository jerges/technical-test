package com.hotelbeds.technicaltest.domain;

public enum SpecialDiscount {
    TOBACCO("tobacco", 50);

    private final String productName;
    private final Integer specialDiscount;

    SpecialDiscount(String productName, Integer specialDiscount) {
        this.productName = productName;
        this.specialDiscount = specialDiscount;
    }

    public String getProductName() {
        return productName;
    }

    public Integer getSpecialDiscount() {
        return specialDiscount;
    }
}
