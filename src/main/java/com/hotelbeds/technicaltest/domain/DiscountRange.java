package com.hotelbeds.technicaltest.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class DiscountRange implements Serializable {

    private final BigDecimal from;

    private final BigDecimal to;
}
