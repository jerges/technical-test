package com.hotelbeds.technicaltest.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class DiscountItem implements Serializable, Comparable<DiscountItem> {

    private final DiscountRange discountRange;
    private final Integer discount;


    @Override
    public int compareTo(final DiscountItem o) {
        return discount.compareTo(o.discount);
    }
}
