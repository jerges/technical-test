package com.hotelbeds.technicaltest.domain;

import java.math.BigDecimal;

public enum Province {
    BAL("(Baleares)", BigDecimal.valueOf(18.3)),
    CAN("(Canarias)", BigDecimal.valueOf(4.7)),
    CYM("(Ceuta y Melilla)", BigDecimal.valueOf(8.1)),
    TER("(Teruel)", BigDecimal.valueOf(0.5)),
    RES("(Resto)", BigDecimal.valueOf(21.5)),
    UNDEFINED("undefined", BigDecimal.ZERO);


    private final String description;
    private final BigDecimal tax;


    Province(String description, BigDecimal tax) {
        this.description = description;
        this.tax = tax;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public BigDecimal getTotalAmount(final BigDecimal amount) {
        return amount.multiply(this.getTax()).divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_UP).add(amount);
    }
}
