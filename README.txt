Para interesados:

El proyecto se empaqueta en un JAR, es una aplicación Spring Boot con lo que podrán ejecutar el programar con el siguientes comando:

java -jar technical-test-0.0.1-SNAPSHOT.jar

technical-test-0.0.1-SNAPSHOT.jar se encuentra en la carpeta target que se autogenera al compilar el proyecto.

Para probar el sistema basta con ejecutar desde el navegador esta URL:

http://localhost:8080/api/item-price-by-state?itemDescription=Laptop&itemPrice=2029&numberOfItems=&state=BAL

Se realiza por una petición GET con los siguientes parámetros:

itemDescription (Descripción del producto)
itemPrice (Precio del producto)
numberOfItems (Cantidad del Producto)
state (Provincia)

IMPORTANTE: La aplicación no utiliza ningun tipo de serguridad, con lo que no será necesario pasar  ningún token,
o algún otro tipo de parámetro de seguridad.

##################################### TEST #####################################

Se realizaron test unitarios durante el desarrollo, NO se implementaron ningún test de integración dado a que no
se hizo uso de ninguna base de datos ni de algún otro servicio.

################################# A TOMAR EN CUENTA ###################################

1) He implementado diferente formas de almacenar los datos, desde el enum hasta un bean que paso por inyección de
dependencia, siendo éste por defecto singleton. Lo he realizado de esta manera para dar a entender que manejo
ambos "mecanismos" ya que según la necesidad una opción puede ser más interesante implementar que la otra.

2) He implementado una exception class de tipo runtime para gestionar en este caso una provincia que no este en nuestro
enum y gestionar dicho error. Al haber implementado esta solución con un API REST se debería crear un Handler para
gestionar todas las posibles excepciones que pueda salir, gestionadola "customizadamente" utilizando los condigos
Http, no lo implementa ya que no era un requerimiento del test, pero hago esta aclaración para dejar claro que haria falta.

3)He trabajado con objetos inmutables y he creado los test unitarios utilizando la libreria EqualsVerifier, muy útil según mi parecer.




